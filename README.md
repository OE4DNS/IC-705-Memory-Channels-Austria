> :warning: **Warnung:** Dieses Repository ist veraltet!<br />
Die aktuellen Codeplugs findet man unter: https://oe3lrt.gitlab.io/memory-channels-processor und https://gitlab.com/oe3lrt/memory-channels-processor

> :warning: **Warning:** This repository is out of date!<br />
The current codeplugs can be found at: https://oe3lrt.gitlab.io/memory-channels-processor and https://gitlab.com/oe3lrt/memory-channels-processor

# ~~IC-705-Memory-Channels-Austria~~

~~Austrian 2m/70cm FM and D-Star hamradio repeater channels ready to import into Icom IC-705 and Icom ID-52E memory.~~

~~Österreichische 2m/70cm FM und D-Star Amateurfunk Relaiskanäle zum Importieren in den Speicher eines Icom IC-705 und Icom ID-52E.~~

**Data from:** 2023-06-04


## ~~Deutsche Anleitung~~

(english description below)

Der Icom IC-705 bietet Speicherplatz für bis zu 500 Relais und Simplexkanäle. Für eine bessere Übersicht können sie in bis zu 100 Gruppen mit wiederum maximal 100 Kanälen sortiert werden, wobei die maximale Anzahl weiterhin bei 500 bleibt. Zusätzlich können außerdem bis zu 2500 D-Star Relais in bis zu 50 Gruppen gespeichert werden. Bei der Auslieferung ist ein Bereich des DV-Speichers bereits mit Relais aus der ganzen Welt, sortiert in geografisch geordnete Gruppen, vorbelegt.

Die hier bereitgestellten CSV Dateien enthalten alle österreichischen 10m, 6m, 2m und 70 cm Analog- und Digitalrelais die mit dem IC-705/ID-52E verwendet werden können. Die Daten stammen vom [ÖVSV UKW Referat](https://www.oevsv.at/funkbetrieb/amateurfunkfrequenzen/ukw-referat/maps/). Für die Verwendung im IC-705/ID-52E wurden daraus die verwendbaren Frequenzen und Modi extrahiert.

Die CSV Dateien können als "Codeplugs" entweder über die SD-Karte, oder die von Icom bereitgestellte CS-705/CS-52 Software in einen IC-705/ID-52E importiert werden. Ob sie dabei in einzelne Gruppen importiert, oder zusammengefasst werden, bleibt den Nutzern überlassen. Vermutlich sind die Daten auch mit anderen Icom Funkgeräten der gleichen Generation kompatibel, das wurde aber noch nicht getestet.

Cross-Band Relais (z.B. Zugspitzenrelais) sind im Moment noch nicht enthalten, dazu muss das Speicherlayout noch genauer untersucht werden.


### Folgende Dateien stehen zur Verfügung

* **Österreichische FM Amateurfunkrelais im 2m-Band:** [Austria-2m-repeaters.csv](Austria-2m-repeaters.csv)  
  Import in CS-705 / CS-52 mittels `Memory CH` > Rechtsklick auf z.B. `00` > `Import` > `Group`
* **Österreichische FM Amateurfunkrelais im 70cm-Band:** [Austria-70cm-repeaters.csv](Austria-70cm-repeaters.csv)  
  Import in CS-705 / CS-52 mittels `Memory CH` > Rechtsklick auf z.B. `00` > `Import` > `Group`
* **Österreichische FM Amateurfunkrelais im 6 und 10m-Band:** [Austria-6_and_10m-repeaters.csv](Austria-6_and_10m-repeaters.csv)  
  Import in CS-705 / CS-52 mittels `Memory CH` > Rechtsklick auf z.B. `00` > `Import` > `Group`
* **Österreichische D-Star Amateurfunkrelais im 2m und 70cm-Band:** [Austria-D-Star-repeaters.csv](Austria-D-Star-repeaters.csv)  
  Import in CS-705 / CS-52 mittels `Digital` > `Repeater List` > Rechtsklick auf z.B. `21` > `Import` > `Group`
* **Alle Amateurfunkrelais-Frequenzen im 2m und 70cm Band:** [general-repeater-channels.csv](general-repeater-channels.csv)

> **HINWEIS**:
> Es kann sein, dass in der Konfigurationssoftware CS-705/CS-52 nach dem Import der D-Star Repeater Warnungen angezeigt werden und die Konfiguration dann nicht gespeichert werden kann bzw. die Einträge entfernt werden. +
> 
> ![Warnungen in der Konfigurationssoftware](configuration_warning.png)
> 
> Das Problem ist, dass innerhalb der anderen Repeater-Gruppen ein oder mehrere doppelte Einträge existieren.
Ausschlaggebend sind die Werte im Feld `Repeater Call Sign`.
> 
> Das Problem lässt sich lösen, indem man alle anderen Gruppen von Repeatern auf Einträge mit identen Callsigns absucht und dann sich für jeweils einen der Einträge entscheidet.
>
> Es wurde berichtet dass der Import der CSV Dateien in die Konfigurationssofware CS-705 fehlschlägt, wenn das Windows Betriebssystem auf eine andere Sprache als Deutsch eingestellt ist. Das liegt an den Einstellungen für das Dezimalzeichen bei der Frequenzangabe in Mhz. Im deutschen ist diese ein Komma "," während im Englischen dafür ein Punkt "." verwendet wird. Dies kann man durch "Suchen und Ersetzen" reparieren, oder Windows auf Deutsch einstellen.


### Generierung der CSV-Dateien

Die Generierung der CSV-Dateien erfolgt mittels der Software [KNIME](https://www.knime.com/) unter Zuhilfenahme des bereitgestellten [KNIME Workflows](KNIME-Repeaters-Austria-ICOM.knar).  
Die zugrundeliegenden Daten werden dabei direkt von der [ÖVSV Website](https://repeater.oevsv.at/static/lists_raw_data_local.xlsx) bezogen.  
Details über die Benutzung von KNIME findet man im [KNIME Handbuch](https://docs.knime.com/latest/analytics_platform_workbench_guide).

![KNIME workflow](knime_workflow.png)

Folgende Schritte sind dabei durchzuführen:

* Knime starten und die Workflows importieren:  
  `File` > `Import KNIME Workflow...` > `Select file` > Select [KNIME-Repeaters-Austria-ICOM.knar](KNIME-Repeaters-Austria-ICOM.knar) > `Finish`
* Eine neue Gruppe `Repeaters Austria ICOM` enthält die einzelnen Workflows
* Einen Workflow mit Doppelklick öffnen
* Mittels Rechtsklick auf die letzte Komponente `CSV Writer` > `Execute` den Workflow starten.  
  Nach der Ausführung sollte die Statusanzeige unter der Komponente von rot auf grün gewechselt sein
* Das generierte File liegt nun unter `<Local path>\data\Austria-<Band>-repeaters.csv` (den Pfad `Local path` erhält man mit Rechtsklick auf den gewünschten Workflow > `Copy Location` > `Local path`)


## ~~English Description~~

The Icom IC-705 provides memory for up to 500 repeater and simplex channels. For a better overview, they can be sorted into up to 100 groups, again with a maximum of 100 channels each, while the overall number remains at 500. In addition, up to 2500 D-Star repeaters can also be stored in up to 50 groups. On delivery, a section of the DV memory is already pre-populated with repeaters from all over the world, sorted into geographically ordered groups.

The CSV files provided here contain all Austrian 2m and 70 cm analog and digital repeaters that can be used with the IC-705/ID-52E. The data originates from the [ÖVSV UKW Referat](https://www.oevsv.at/funkbetrieb/amateurfunkfrequenzen/ukw-referat/maps/). For the use in the IC-705/ID-52E only usable frequencies and modes were extracted.

The CSV files can be imported into an IC-705/ID-52E as "codeplugs" either via the SD card, or the CS-705/CS-52 software provided by Icom. Whether they are imported into individual groups, or combined, is up to the user. Presumably the data is also compatible with other Icom radios of a similar generation, but this has not been tested yet.

Cross-band repeaters (e.g. the repeater at the Zugspitze) are not included at the moment, the memory layout has to be examined more closely for this.


### The following files are provided

* **Austrian FM Hamradio repeaters in the 2m-Band:** [Austria-2m-repeaters.csv](Austria-2m-repeaters.csv)  
  Import in CS-705 / CS-52 using `Memory CH` > right-click e.g. `00` > `Import` > `Group`
* **Austrian FM Hamradio repeaters in the 70cm-Band:** [Austria-70cm-repeaters.csv](Austria-70cm-repeaters.csv)  
  Import in CS-705 / CS-52 using `Memory CH` > right-click e.g. `00` > `Import` > `Group`
* **Austrian FM Hamradio repeaters in the 6 and 10m-Band:** [Austria-6_and_10m-repeaters.csv](Austria-6_and_10m-repeaters.csv)  
  Import in CS-705 / CS-52 using `Memory CH` > right-click e.g. `00` > `Import` > `Group`
* **Austrian D-Star repeaters in the 2m and 70cm-Band:** [Austria-D-Star-repeaters.csv](Austria-D-Star-repeaters.csv)  
  Import in CS-705 / CS-52 using `Digital` > `Repeater List` > right-click e.g. `21` > `Import` > `Group`
* **All Hamradio repeater frequencies in the 2m and 70cm Band:** [general-repeater-channels.csv](general-repeater-channels.csv)

> **HINT**:
> It can happen that in the configuration software CS-705/CS-52 warnings are displayed after the import of the D-Start repeaters.
> As a result the new configuration can't be saved or the new entries are deleted.
>
> ![Warnings in the configuration software](configuration_warning.png)
>
> The problem is, that in one of the other repeater groups duplicates exist.
> The matching is done via the field `Repeater Call Sign`.
>
> To solve it, search in all other repeater groups for identical items and then choose which entry to keep or delete.
>
> It has been reported that the import of CSV files into the configuration software CS-705 fails if the Windows operating system is set to a language other than German. This is due to the settings for the decimal point for the frequency in MHz. In German this is a comma "," while in English a dot "." is used for it. This can be repaired by "Search and Replace", or Windows can be set to German instead.


### Generation of the CSV files

For the generation of the CSV files a software called [KNIME](https://www.knime.com/) is used in combination with the provided [KNIME workflow](KNIME-Repeaters-Austria-ICOM.knar).  
The underlying data will be downloaded during the process from the [ÖVSV Website](https://repeater.oevsv.at/static/lists_raw_data_local.xlsx).  
Details about the usage of KNIME can be found in the [KNIME manual](https://docs.knime.com/latest/analytics_platform_workbench_guide).

![KNIME workflow](knime_workflow.png)

The following steps have to be taken:

* Start Knime and import the workflows:  
  `File` > `Import KNIME Workflow...` > `Select file` > Select [KNIME-Repeaters-Austria-ICOM.knar](KNIME-Repeaters-Austria-ICOM.knar) > `Finish`
* A new group called `Repeaters Austria ICOM` now contains the separate workflows
* Open a workflow with a double click
* Right-click on the last component `CSV Writer` > `Execute` to run the workflow.  
  After the execution the status indicator below the component should turn from red to green
* The generated file now is available under `<Local path>\data\Austria-<Band>-repeaters.csv` (get the path `Local path` with a right click on the desired workflow > `Copy Location` > `Local path`)
